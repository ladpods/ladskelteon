//
//  LADViewController.h
//  LADSkeleton
//
//  Created by obellerose on 07/21/2016.
//  Copyright (c) 2016 obellerose. All rights reserved.
//

@import UIKit;

@interface LADViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end
