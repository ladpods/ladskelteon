//
//  LADAppDelegate.h
//  LADSkeleton
//
//  Created by obellerose on 07/21/2016.
//  Copyright (c) 2016 obellerose. All rights reserved.
//

@import UIKit;

@interface LADAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
