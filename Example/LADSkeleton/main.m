//
//  main.m
//  LADSkeleton
//
//  Created by obellerose on 07/21/2016.
//  Copyright (c) 2016 obellerose. All rights reserved.
//

@import UIKit;
#import "LADAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LADAppDelegate class]));
    }
}
