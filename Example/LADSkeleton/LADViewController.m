//
//  LADViewController.m
//  LADSkeleton
//
//  Created by obellerose on 07/21/2016.
//  Copyright (c) 2016 obellerose. All rights reserved.
//

#import "LADViewController.h"
#import "LADSkeletonView.h"
#import "LADParagraphObject.h"

@interface LADViewController ()

@end

@implementation LADViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    LADParagraphObject * obj = [[LADParagraphObject alloc] init];
    obj.numberOfLine = 3;
    NSMutableArray * paragraphArray = [[NSMutableArray alloc] init];
    [paragraphArray addObject:obj];
    
    LADParagraphObject * obj2 = [[LADParagraphObject alloc] init];
    obj2.numberOfLine = 5;
    obj2.lastLineWidth = 200;
    obj2.lineColor = [UIColor redColor];
    [paragraphArray addObject:obj2];
    
    LADParagraphObject * obj3 = [[LADParagraphObject alloc] init];
    obj3.numberOfLine = 4;
    obj3.lastLineWidth = 150;
    obj3.marginTop = 40;
    obj3.lineColor = [UIColor lightGrayColor];
    [paragraphArray addObject:obj3];
    
    LADSkeletonView * skeleton = [[LADSkeletonView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 2*16, self.containerView.frame.size.height) paragraphes:paragraphArray andMarginsTop:50 bottom:30 left:16 right:16 inContainerView:self.containerView];
    [skeleton shimmering];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
