//
//  LADShimmeringView.m
//  LASkeleton
//
//  Created by BELLEROSE Odile on 21/07/2016.
//  Copyright © 2016 BELLEROSE Odile. All rights reserved.
//

#import "LADShimmeringView.h"
#import "FBShimmeringLayer.h"


@implementation LADShimmeringView
@synthesize contentView = _contentView;

- (void)setContentView:(UIView *)contentView
{
    if (contentView != _contentView) {
        _contentView = contentView;
        [self addSubview:contentView];
        
        [_contentView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
                                                         attribute:NSLayoutAttributeLeading
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeLeading
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
                                                         attribute:NSLayoutAttributeTrailing
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeTrailing
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        ((FBShimmeringLayer *)self.layer).contentLayer = contentView.layer;
    }
}

@end
