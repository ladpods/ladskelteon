//
//  LAParagraph.h
//  LASkeleton
//
//  Created by BELLEROSE Odile on 20/07/2016.
//  Copyright © 2016 BELLEROSE Odile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LADParagraphObject : NSObject
@property (nonatomic, assign) NSInteger lineHeight;
@property (nonatomic, assign) NSInteger lineSpacing;
@property (nonatomic, strong) UIColor * lineColor;
@property (nonatomic, assign) CGFloat lastLineWidth;
@property (nonatomic, assign) NSInteger numberOfLine;
@property (nonatomic, assign) NSInteger marginTop;
@end
