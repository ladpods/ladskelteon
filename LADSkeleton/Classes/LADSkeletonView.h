//
//  LASkeletonView.h
//  LASkeleton
//
//  Created by BELLEROSE Odile on 20/07/2016.
//  Copyright © 2016 BELLEROSE Odile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LADShimmeringView.h"

@interface LADSkeletonView : UIView
@property (nonatomic, strong) NSMutableArray * paragraphes;
@property (nonatomic, assign) NSInteger marginTop;
@property (nonatomic, assign) NSInteger marginBottom;
@property (nonatomic, assign) NSInteger marginLeft;
@property (nonatomic, assign) NSInteger marginRight;
@property (nonatomic, strong) LADShimmeringView *shimmeringView;
- (id)initWithFrame:(CGRect)frame paragraphes:(NSMutableArray*)paragraphes andMarginsTop:(NSInteger)top bottom:(NSInteger)bottom left:(NSInteger)left right:(NSInteger)right inContainerView:(UIView*)containerView;
- (void)shimmering;
@end
