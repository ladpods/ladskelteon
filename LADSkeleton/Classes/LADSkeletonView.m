//
//  LASkeletonView.m
//  LASkeleton
//
//  Created by BELLEROSE Odile on 20/07/2016.
//  Copyright © 2016 BELLEROSE Odile. All rights reserved.
//

#import "LADSkeletonView.h"
#import "LADParagraphObject.h"

@interface LADSkeletonView ()
@property (nonatomic, strong) UIView * previousLineView;
@property (nonatomic, strong) UIView * containerView;
@end

@implementation LADSkeletonView

- (id)initWithFrame:(CGRect)frame paragraphes:(NSMutableArray*)paragraphes andMarginsTop:(NSInteger)top bottom:(NSInteger)bottom left:(NSInteger)left right:(NSInteger)right inContainerView:(UIView*)containerView
{
    self = [super initWithFrame:frame];
  
    if(self)
    {
        self.paragraphes = paragraphes;
        self.marginTop = top;
        self.marginBottom = bottom;
        self.marginLeft = left;
        self.marginRight = right;
        self.containerView = containerView;
    
        [self buildSkeleton];
    }
    
    return self;
}

- (void)buildSkeleton
{
    for (id object in self.paragraphes)
    {
        if([object isKindOfClass:[LADParagraphObject class]])
        {
            LADParagraphObject * paragraph = (LADParagraphObject*)object;
            
            for (int i = 0 ; i < paragraph.numberOfLine; i++)
            {
                UIView * lineView ;
                lineView = [[UIView alloc] init];
                [lineView setBackgroundColor:paragraph.lineColor];
                [self addSubview:lineView];
                [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
                if(i == 0 && paragraph == [self.paragraphes firstObject])
                {
                    [self createFullLineConstraintWithItem:lineView toItem:self marginTop:self.marginTop andParagraph:paragraph];
                }
                else if(i == 0 && paragraph != [self.paragraphes firstObject])
                {
                     [self createFullLineConstraintWithItem:lineView toItem:self.previousLineView marginTop:paragraph.marginTop andParagraph:paragraph];
                }
                else if (i == paragraph.numberOfLine - 1)
                {
                    [self createLastLineConstraintWithItem:lineView toItem:self.previousLineView andParagraph:paragraph];
                }
                else
                {
                    [self createFullLineConstraintWithItem:lineView toItem:self.previousLineView marginTop:paragraph.lineSpacing andParagraph:paragraph];
                }
                
                self.previousLineView = lineView;
                [self.previousLineView layoutIfNeeded];
            }
        }
    }
}

- (void)shimmering
{
    if (self.containerView)
    {
        self.shimmeringView = [[LADShimmeringView alloc] init];
        
        [self.containerView addSubview:self.shimmeringView];
        [self.shimmeringView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.shimmeringView
                                                                       attribute:NSLayoutAttributeTop
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.containerView
                                                                       attribute:NSLayoutAttributeTop
                                                                      multiplier:1.0
                                                                        constant:0.0]];
        
        [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.shimmeringView
                                                                       attribute:NSLayoutAttributeLeading
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.containerView
                                                                       attribute:NSLayoutAttributeLeading
                                                                      multiplier:1.0
                                                                        constant:0.0]];
        
        [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.shimmeringView
                                                                       attribute:NSLayoutAttributeBottom
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.containerView
                                                                       attribute:NSLayoutAttributeBottom
                                                                      multiplier:1.0
                                                                        constant:0.0]];
        
        [self.containerView addConstraint:[NSLayoutConstraint constraintWithItem:self.shimmeringView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.containerView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1.0
                                                                        constant:0.0]];
        
        self.shimmeringView.contentView = self;
        self.shimmeringView.shimmering = YES;
    }
}

- (void)createLastLineConstraintWithItem:(UIView*)firstItem toItem:(UIView*)secondItem andParagraph:(LADParagraphObject*)paragraph
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:firstItem
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:secondItem
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0
                                                      constant:paragraph.lineSpacing]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:firstItem
                                                     attribute:NSLayoutAttributeLeading
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeading
                                                    multiplier:1.0
                                                      constant:self.marginLeft]];
    
    [firstItem addConstraint:[NSLayoutConstraint constraintWithItem:firstItem
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:paragraph.lineHeight]];
    
    [firstItem addConstraint:[NSLayoutConstraint constraintWithItem:firstItem
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:paragraph.self.lastLineWidth]];
}


- (void)createFullLineConstraintWithItem:(UIView*)firstItem toItem:(UIView*)secondItem marginTop:(CGFloat)marginTop andParagraph:(LADParagraphObject*)paragraph
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:firstItem
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:secondItem
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0
                                                      constant:marginTop]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:firstItem
                                                     attribute:NSLayoutAttributeLeading
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeading
                                                    multiplier:1.0
                                                      constant:self.marginLeft]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                     attribute:NSLayoutAttributeTrailing
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:firstItem
                                                     attribute:NSLayoutAttributeTrailing
                                                    multiplier:1.0
                                                      constant:self.marginRight]];
    
    
    [firstItem addConstraint:[NSLayoutConstraint constraintWithItem:firstItem
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:paragraph.lineHeight]];
}

@end
