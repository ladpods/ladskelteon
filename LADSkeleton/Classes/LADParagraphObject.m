//
//  LAParagraph.m
//  LASkeleton
//
//  Created by BELLEROSE Odile on 20/07/2016.
//  Copyright © 2016 BELLEROSE Odile. All rights reserved.
//

#import "LADParagraphObject.h"

@implementation LADParagraphObject

- (id)init
{
    self = [super init];
    
    if(self)
    {
        self.lineHeight = 6;
        self.lineSpacing = 20;
        self.lineColor = [UIColor blackColor];
        self.lastLineWidth = 100;
        self.numberOfLine = 4;
        self.marginTop = 30;
    }
    
    return self;
}

@end
