# LADSkeleton

[![CI Status](http://img.shields.io/travis/obellerose/LADSkeleton.svg?style=flat)](https://travis-ci.org/obellerose/LADSkeleton)
[![Version](https://img.shields.io/cocoapods/v/LADSkeleton.svg?style=flat)](http://cocoapods.org/pods/LADSkeleton)
[![License](https://img.shields.io/cocoapods/l/LADSkeleton.svg?style=flat)](http://cocoapods.org/pods/LADSkeleton)
[![Platform](https://img.shields.io/cocoapods/p/LADSkeleton.svg?style=flat)](http://cocoapods.org/pods/LADSkeleton)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LADSkeleton is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LADSkeleton"
```
## Utilisation
  
```objc
NSMutableArray * paragraphArray = [[NSMutableArray alloc] init];

//Create some paragraphes

LADParagraphObject * obj = [[LADParagraphObject alloc] init];
obj.numberOfLine = 3;
obj.lastLineWidth = 100;
obj.lineColor = [UIColor publicGrey80];
[paragraphArray addObject:obj];
 
LADParagraphObject * obj2 = [[LADParagraphObject alloc] init];
obj2.numberOfLine = (IS_IPAD ? 6 : 4);
obj2.lastLineWidth = 150;
obj2.lineColor = [UIColor publicGrey95];
obj2.marginTop = 47;
[paragraphArray addObject:obj2];
        
UIView * containerView = [...]; // Container view for Skeleton view
LADSkeletonView * skeletonView = [[LADSkeletonView alloc] initWithFrame:frame paragraphes:paragraphArray andMarginsTop:35 bottom:0 left:16 right:16 inContainerView:containerView];

//To apply shimmering animation call shimmering function :
[skeletonView shimmering];

//if you want to add the skeletonView in a specific view without shimmering animation :
[containerView addSubview:skeletonView] 
```

## Author

obellerose, extia-odile.bellerose@lagardere-active.com

## License

LADSkeleton is available under the MIT license. See the LICENSE file for more info.
